<?php

namespace ArticleStat;

class ArticleStatCategorySelect
{
    public static function show($categoryId)
    {
        self::includeSelect2();

        ?>
        <form action="/wp-admin/admin.php" method="GET">
            <input type="hidden" name="page" value="<?=$_REQUEST['page']?>" />

            <?php
            $dropdownArgs = array(
                'hide_empty'       => 0,
                'hide_if_empty'    => false,
                'taxonomy'         => ArticleStatConstants::KB_POST_TYPE_TAXONOMY,
                'id'               => 'articleStatCategorySelect',
                'name'             => ArticleStatConstants::KB_CATEGORY_SELECT_NAME,
                'orderby'          => 'name',
                'selected'         => $categoryId,
                'hierarchical'     => true,
                'show_option_none' => __('None'),
            );
            wp_dropdown_categories($dropdownArgs);
            ?>
        <br>
        <br>
        <input type="submit" class="button button-primary" value="Выбрать">
        </form>
        <br>

    <?php }


    public static function includeSelect2()
    {
        wp_enqueue_style(
            'select2-css',
            get_template_directory_uri() . '/js/select2/select2.min.css',
        );
        wp_enqueue_script(
            'select2-js',
            get_template_directory_uri() . '/js/select2/select2.full.min.js',
            array('jquery')
        );
        wp_enqueue_script(
            'select2-ru-js',
            get_template_directory_uri() . '/js/select2/i18n/ru.js',
            array('select2-js')
        );
        ?>

        <script>
            $(function() {
                let categorySelect = $('#articleStatCategorySelect');

                if (categorySelect.length > 0) {
                    categorySelect.select2({
                        width: '100%',
                        language: "ru"
                    });
                }
            });
        </script>
    <?php
    }
}

