<?php

namespace ArticleStat;

use WP_Query;

class ArticleStat
{
    private $categoryId;
    private $posts;

    public function main()
    {
        $this->getCategoryIdFromPost();
        ArticleStatCategorySelect::show($this->categoryId);
        $this->getPostsFromCategory();
        $this->sortPostsByViews();
        $this->showTable();
    }

    private function getCategoryIdFromPost()
    {
        $this->categoryId = $_REQUEST[ArticleStatConstants::KB_CATEGORY_SELECT_NAME] ?? 0;
    }

    private function getPostsFromCategory()
    {
        $posts = [];
        $args = array(
            'posts_per_page' => -1,
            'post_type' => ArticleStatConstants::KB_POST_TYPE,
        );

        if ($this->categoryId > 0) {
            $args['tax_query'] = [
                [
                    'taxonomy' => ArticleStatConstants::KB_POST_TYPE_TAXONOMY,
                    'field' => 'term_id',
                    'terms' => $this->categoryId,
                    'include_children' => true
                ]
            ];
        }

        $postsQuery = new WP_Query($args);

        if ($postsQuery->have_posts()) {
            while ($postsQuery->have_posts()) {
                $postsQuery->the_post();
                $postId = get_the_ID();

                $posts[] = [
                    'id' => $postId,
                    'title' => get_the_title(),
                    'url' => get_the_permalink(),
                    ArticleStatConstants::KB_VIEWS_FIELD => get_post_meta(
                        $postId,
                        ArticleStatConstants::KB_VIEWS_FIELD,
                        true
                    )
                ];
            }
        }

        $this->posts = $posts;
    }

    private function sortPostsByViews()
    {
        $keys = array_column($this->posts, ArticleStatConstants::KB_VIEWS_FIELD);
        array_multisort($keys, SORT_DESC, $this->posts);
    }

    private function showTable()
    {
        if (empty($this->posts)) {
            return;
        }

        $counter = 0;

        echo '<table>';
        echo '<tr style="font-weight:bold;"><td>№</td><td>Статья БЗ</td><td>Просмотры</td></tr>';
        foreach ($this->posts as $post) {
            echo '<tr>';
            echo '<td>' . ++$counter . '</td>';
            echo '<td><a href="' . $post['url'] . '">' . $post['title'] . '</a></td>';
            echo '<td>' . $post[ArticleStatConstants::KB_VIEWS_FIELD] . '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
}
