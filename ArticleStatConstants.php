<?php

namespace ArticleStat;

class ArticleStatConstants
{
    const KB_POST_TYPE = 'epkb_post_type_1';
    const KB_POST_TYPE_TAXONOMY = 'epkb_post_type_1_category';
    const KB_VIEWS_FIELD = 'kb_popularity';
    const KB_CATEGORY_SELECT_NAME = 'kb_category';
}
